use ground_control::{
    architect::{import::*, *},
    ControlMason,
};
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io::BufReader;
use std::iter::FromIterator;

pub struct FileGet;

impl FileGetter for FileGet {
    fn get(&mut self, path: &str) -> Result<BufReader<File>, std::io::Error> {
        let file = File::open(path)?;
        Ok(BufReader::new(file))
    }
}

pub struct SettingsMason {
    pub control: ControlMason,
}

impl Default for SettingsMason {
    fn default() -> SettingsMason {
        SettingsMason {
            control: ControlMason::default(),
        }
    }
}

impl StoneMason for SettingsMason {
    fn handle_stones(
        &mut self,
        arch: &mut Architect,
        map: &mut HashMap<String, Vec<usize>>,
    ) -> HashSet<usize> {
        let mut res = self.control.handle_stones(arch, map);
        for s in match map.get("meta") {
            Some(v) => v,
            None => return res,
        } {
            res.insert(*s);
        }
        res
    }
}

pub struct MetaMason {
    pub import: ImportMason<FileGet>,
    pub settings: SettingsMason,
}

impl Default for MetaMason {
    fn default() -> Self {
        Self::new()
    }
}

impl MetaMason {
    pub fn new() -> MetaMason {
        MetaMason {
            import: ImportMason::new(FileGet),
            settings: SettingsMason::default(),
        }
    }
}

impl StoneMason for MetaMason {
    fn handle_stones(
        &mut self,
        arch: &mut Architect,
        map: &mut HashMap<String, Vec<usize>>,
    ) -> HashSet<usize> {
        let rem_i = self.import.handle_stones(arch, map);
        let rem_s = self.settings.handle_stones(arch, map);
        HashSet::from_iter(rem_i.iter().chain(rem_s.iter()).cloned())
    }
}
